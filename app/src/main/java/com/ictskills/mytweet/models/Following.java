package com.ictskills.mytweet.models;

import com.google.gson.stream.JsonWriter;

import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.Gson;

import java.util.UUID;

import static java.lang.String.valueOf;

/**
 * Created by Aine on 22/12/2015.
 */
public class Following
{
    public Tweeter follower;
    public Tweeter followee;
    public String id;

    private static final String JSON_ID = "id";
    private static final String JSON_FOLLOWER= "follower";
    private static final String JSON_FOLLOWEE= "followee";
    //private static final String JSON_FOLLOWERID= "followerId";
   // private static final String JSON_FOLLOWEEID= "followeeId";


    public Following (Tweeter sourceTweeter, Tweeter targetTweeter)
    {
        this.id = UUID.randomUUID().toString();
        follower = sourceTweeter;
        followee = targetTweeter;
    }

    // JSON Tweet constructor
    public Following (JSONObject json) throws JSONException {

        id = json.getString(JSON_ID);

        follower = (Tweeter) json.get(JSON_FOLLOWER);
        followee = (Tweeter) json.get(JSON_FOLLOWEE);
        //follower.id = json.getString(JSON_FOLLOWERID);
       // followee.id = json.getString(JSON_FOLLOWEEID);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, id);
        json.put(JSON_FOLLOWER, follower);
        json.put(JSON_FOLLOWEE, followee);
        //json.put(JSON_FOLLOWERID, follower);
       // json.put(JSON_FOLLOWEEID, followee);
        return json;
    }


}

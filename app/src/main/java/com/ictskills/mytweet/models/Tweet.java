package com.ictskills.mytweet.models;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

import static com.ictskills.android.helpers.LogHelpers.info;

public class Tweet
{
    /**
     * The id is required immediately on the client-side
     * We send to id the server in the http call and configure the server to allow this practice
     */
    public String id;
    public String tweetText;
    public Long datestamp;
    public String strDate;
    public int charCount;
    public String tweeterTweetText;
    public String tweetContact;
    public boolean buttonClicked = false;  // links to tweetButton and TweetPager onOptionsItemSelected


    private static final String JSON_ID = "id";
    private static final String JSON_CHARCOUNT = "charCount";
    private static final String JSON_DATESTAMP = "date";
    private static final String JSON_TWEETTEXT = "tweetText";
    private static final String JSON_TWEETERTWEETTEXT = "tweeterTweetText";

    // Tweet constructor method
    public Tweet() {
        id = UUID.randomUUID().toString();
        charCount = 140;
        datestamp = new Date().getTime();
        tweetText = "";
        tweeterTweetText = "";
    }

    // JSON Tweet constructor
    public Tweet(JSONObject json) throws JSONException {

        id = json.getString(JSON_ID);
        charCount = json.getInt(JSON_CHARCOUNT);
        datestamp = Long.valueOf(json.getString(JSON_DATESTAMP));
        tweetText = json.getString(JSON_TWEETTEXT);
        tweeterTweetText = json.getString(JSON_TWEETERTWEETTEXT);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, id);
        json.put(JSON_CHARCOUNT, charCount);
        json.put(JSON_TWEETTEXT, tweetText);
        json.put(JSON_DATESTAMP, datestamp);
        json.put(JSON_TWEETERTWEETTEXT, tweeterTweetText);
        return json;
    }

    public void setTweetText(String tweetText) {
        this.tweetText = tweetText;
    }

    public String getTweet() {
        String dateString = DateFormat.getDateTimeInstance().format(datestamp);
        String totalTweet = "Tweet: " + tweetText + " Date: " + dateString;
        return totalTweet;
    }

    public String timeSince(Tweet tweet)
    {
        String min = "less than one minute ago";

        PeriodFormatter formatter = new PeriodFormatterBuilder()
                .printZeroNever()
                .appendWeeks()
                .appendSuffix(" week", " weeks")
                .appendSeparator(", ")
                .appendDays()
                .appendSuffix(" day", " days")
                .appendSeparator(", ")
                .appendHours()
                .appendSuffix(" hour", " hours")
                .appendSeparator(", ")
                .appendMinutes()
                .appendSuffix(" minute", " minutes")
                .toFormatter();

        try {
            DateTime now = new DateTime();
            DateTime tweetTime = new DateTime(tweet.datestamp);
            Interval interval = new Interval(tweetTime, now);
            Period period = interval.toPeriod();
            if (period.getMinutes() <= 1)
            {
                return min;
            }
            else
            {
                String timer = formatter.print(period) + " ago";
                return timer;
            }
        } catch (Exception e) {
            info(this, "Error getting time: " + e.getMessage());
            return min;
        }
    }
}

package com.ictskills.mytweet.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Blob;
import java.util.UUID;

public class Tweeter
{
    public String id;
    public String firstName;
    public String lastName;
    public String email;
    public String password;
    public boolean followed;

    private static final String JSON_ID = "id";
    private static final String JSON_FIRSTNAME = "firstName";
    private static final String JSON_LASTNAME = "lastName";
    private static final String JSON_FOLLOWED = "followed";

    public Tweeter(String firstName, String lastName, String email, String password)
    {
        this.id = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    // JSON Tweeter constructor
    public Tweeter(JSONObject json) throws JSONException {

        id = json.getString(JSON_ID);
        firstName = json.getString(JSON_FIRSTNAME);
        lastName = json.getString(JSON_LASTNAME);
        followed = json.getBoolean(JSON_FOLLOWED);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, id);
        json.put(JSON_FIRSTNAME, firstName);
        json.put(JSON_LASTNAME, lastName);
        json.put(JSON_FOLLOWED, followed);
        return json;
    }


    public String getName()
    {
        String name = firstName.toString() + " " + lastName.toString();
        return name;
    }
}

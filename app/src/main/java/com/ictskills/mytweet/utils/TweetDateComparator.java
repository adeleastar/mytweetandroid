package com.ictskills.mytweet.utils;

import com.ictskills.mytweet.models.Tweet;

import java.util.Comparator;
import java.util.Date;



public class TweetDateComparator implements Comparator<Tweet>
{
    @Override
    public int compare(Tweet a, Tweet b)
    {
        Date aDate = new Date(a.datestamp);
        Date bDate = new Date(b.datestamp);
        return aDate.compareTo(bDate);
    }
}
package com.ictskills.mytweet.receivers;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ictskills.mytweet.services.RefreshService;
import com.ictskills.mytweet.utils.NumUtil;


/**
 * If permission set and BootReceiver registered in manifest file ...
 * and application manually launched, then...
 * then BootReceiver.onReceive method will be invoked by system when device started.
 * In this method we set the interval at which the alarm should trigger.
 * This will be either default 15 minutes or a value input by user in preference settings.
 */
public class BootReceiver extends BroadcastReceiver
{
    // Set default interval to 15 minutes
    private static final long DEFAULT_INTERVAL = AlarmManager.INTERVAL_FIFTEEN_MINUTES/60000;
    public static int REQUESTCODE = -1;
    private String tag = "MyTweet";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.i(tag, "In BootReceiver.onReceive");

        // We check for a valid user inputted interval and use it if it exists
        // Otherwise use DEFAULT_INTERVAL - the second parameter here following is a default.
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String key = "refresh_interval";  // This key introduced in res/xml/settings.xml
        long interval = DEFAULT_INTERVAL;
        String value = prefs.getString(key, Long.toString(interval)); // last param is a default val
        // Check if input value is valid and if so this becomes the refresh interval, overwriting the default
        if (NumUtil.isPositiveNumber(value))
        {
            interval = Long.parseLong(value);
            interval *= 60000;//here we convert minutes to milliseconds since input at settings menu is specified in minutes
        }
        // Set an arbitrary minimum interval value of 60 seconds to avoid overloading service.
        interval = interval < 60000 ? 60000 : interval;

        // Prepare an PendingIntent with a view to triggering RefreshService
        PendingIntent operation = PendingIntent.getService(
                context,
                REQUESTCODE,
                new Intent(context, RefreshService.class),
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(operation);//cancel any existing alarms with matching intent
        alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval, operation);

        long appliedInterval = interval/60000;
        Log.i(tag, "Boot receiver alarm repeats every: " + appliedInterval + " minutes.");
    }
}
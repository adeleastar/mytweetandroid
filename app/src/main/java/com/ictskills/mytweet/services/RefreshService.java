package com.ictskills.mytweet.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.ictskills.android.helpers.LogHelpers;
import com.ictskills.mytweet.activities.tweet.TweetList;
import com.ictskills.mytweet.activities.tweet.TweetListFragment;
import com.ictskills.mytweet.containers.FollowPortfolio;
import com.ictskills.mytweet.containers.Portfolio;
import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Tweet;
import com.ictskills.mytweet.models.Tweeter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RefreshService extends IntentService {
    private String tag = "MyTweet";
    MyTweetApp app;
    private Portfolio portfolio;
    private FollowPortfolio followPortfolio;
    private ArrayList<Tweeter> followed;
    private List<Tweet> tweets = new ArrayList<>();
    private Tweeter currentTweeter;

    public RefreshService()
    {
        super("RefreshService");

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        app = (MyTweetApp) getApplication();

        followPortfolio = app.followPortfolio;
        followed = followPortfolio.followed;

        currentTweeter = app.currentTweeter;

        Intent localIntent = new Intent(TweetListFragment.BROADCAST_ACTION);

        getTweets();
        getTweeterTweets();
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    public void getTweeterTweets() {
        for (Tweeter tw : followed) {
            Call<List<Tweet>> call = app.tweetService.getTweets(tw.id);
            try {
                Response<List<Tweet>> response = call.execute();
                tweets = response.body();
            } catch (IOException e) {
                LogHelpers.info(tag, "Failed to retrieve tweet list - network error");
            }
        }}

    public void getTweets() {
        Call<List<Tweet>> call = app.tweetService.getTweets(currentTweeter.id);
        try {
            Response<List<Tweet>> response = call.execute();
            portfolio.tweets = (ArrayList<Tweet>) response.body();
        } catch (IOException e) {
            LogHelpers.info(tag, "Failed to retrieve tweet list - network error");
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        LogHelpers.info(this, "onDestroyed");
    }
}

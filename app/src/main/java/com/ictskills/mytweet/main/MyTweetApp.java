package com.ictskills.mytweet.main;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ictskills.mytweet.containers.AllTweetsPortfolio;
import com.ictskills.mytweet.containers.AllTweetsPortfolioSerializer;
import com.ictskills.mytweet.containers.FollowPortfolio;
import com.ictskills.mytweet.containers.FollowPortfolioSerializer;
import com.ictskills.mytweet.containers.Portfolio;
import com.ictskills.mytweet.containers.PortfolioSerializer;
import com.ictskills.mytweet.containers.TweeterPortfolio;
import com.ictskills.mytweet.containers.TweeterPortfolioSerializer;
import com.ictskills.mytweet.models.Tweet;
import com.ictskills.mytweet.models.Tweeter;
import com.ictskills.mytweet.services.RefreshService;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class MyTweetApp extends Application{

    public Portfolio portfolio;
    public AllTweetsPortfolio allTweetsPortfolio;
    public TweeterPortfolio tweeterPortfolio;
    public FollowPortfolio followPortfolio;
    public String               service_url  = "http://10.0.2.2:9000";   // Standard Emulator IP Address
    //public String               service_url  = "http://10.0.3.2:9000"; // Genymotion IP address
   // public String               service_url  = "https://adeleastar-mytweet.herokuapp.com/";

    public TweetServiceProxy tweetService;
    public boolean tweetServiceAvailable = false;

    public List<Tweeter> tweeters = new ArrayList<>();
    public List<Tweet> tweets = new ArrayList<>();
    public ArrayList<Tweet> allTweets = new ArrayList<>();

    public Tweeter currentTweeter;

    private Timer timer;

    private static final String FILENAME = "portfolio.json";
    private static final String FILENAME1 = "allTweetsPortfolio.json";
    private static final String FILENAME2 = "tweeterportfolio.json";
    private static final String FILENAME3 = "followPortfolio.json";

    /*  Method to launch app, creates portfolio to store tweets and sets Preference Manager to
        defaults
     */
    @Override
    public void onCreate() {
        super.onCreate();

        PortfolioSerializer s = new PortfolioSerializer(this, FILENAME);
        portfolio = new Portfolio(s);

        AllTweetsPortfolioSerializer atps = new AllTweetsPortfolioSerializer(this, FILENAME1);
        allTweetsPortfolio = new AllTweetsPortfolio (atps);


        TweeterPortfolioSerializer tps = new TweeterPortfolioSerializer(this, FILENAME2);
        tweeterPortfolio = new TweeterPortfolio(tps);

        FollowPortfolioSerializer fps = new FollowPortfolioSerializer(this, FILENAME3);
        followPortfolio = new FollowPortfolio(fps);

        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(service_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        tweetService = retrofit.create(TweetServiceProxy.class);

        timer = new Timer();

        refreshTweetList();
    }

    public boolean validTweeter(final String email, final String password) {
        for (Tweeter tweeter : tweeters) {
            if (tweeter.email.equals(email) && tweeter.password.equals(password)) {

                currentTweeter = tweeter;
                tweeterPortfolio.deleteTweeter(currentTweeter);
                return true;
            }
        }
        return false;
    }

    public void refreshTweetList()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // Parmeter 2 is default val in minutes set at 1 minute here for test purposes.
        // The preference key *refresh_interval* is defined in *res/xml/settings.xml*.
        String refreshInterval = prefs.getString("refresh_interval", "1");
        // Precondition: user-input refresh frequency units are minutes.
        // Convert refreshFrequency minutes to milliseconds.
        int refreshFrequency = Integer.parseInt(refreshInterval) * 60 * 1000;
        int initialDelay = 1000; // Set an initial delay of 1 second

        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                startService(new Intent(getBaseContext(), RefreshService.class));
            }
        }, initialDelay, refreshFrequency);
    }
}

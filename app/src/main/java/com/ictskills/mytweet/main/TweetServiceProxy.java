package com.ictskills.mytweet.main;

import com.ictskills.mytweet.models.Following;
import com.ictskills.mytweet.models.Tweet;
import com.ictskills.mytweet.models.Tweeter;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface TweetServiceProxy
{
    // Create a tweet
    @POST("/api/tweeters/{id}/tweets")
    Call<Tweet>createTweet(@Path("id") String id, @Body Tweet tweet);

    // Get a tweetersTweets
    @GET("/api/tweeters/{id}/tweets")
    Call<List<Tweet>> getTweets(@Path("id") String id);

    // Get a tweet
    @GET("/api/tweeters/{id}/tweets/{tweetId}")
    Call<Tweet> getTweet(@Path("id") String id, @Path("tweetId") String tweetId);

    // Delete a tweet
    @DELETE("/api/tweeters/{id}/tweets/{tweetId}")
    Call<Tweet> deleteTweet(@Path("id") String id, @Path("tweetId") String tweetId);

    // Delete a tweeters tweets
    @DELETE("/api/tweeters/{id}/tweets")
    Call<String>deleteTweets(@Path("id") String id);

    // Get all tweets
    @GET("/api/tweets")
    Call<List<Tweet>> getAllTweets();

    // Delete all Tweets
    @DELETE("/api/tweets")
    Call<String> deleteAllTweets();

    //Create a tweeter
    @POST("/api/tweeters")
    Call<Tweeter> createTweeter(@Body Tweeter tweeter);

    // Get all tweeters
    @GET("/api/tweeters")
    Call<List<Tweeter>> getAllTweeters();

    // Get a Tweeter
    @GET("/api/tweeters/{id}")
    Call<Tweeter> getTweeter(@Path("id") String id);

    //Create Following
    @POST("api/tweeters/{id}/followings/{followeeId}")
    Call<Following> createFollowing(@Path("id") String id, @Path("followeeId") String followeeId);

    //Delete Following
    @DELETE("api/tweeters/{id}/followings/{followeeId}")
    Call<Following> deleteFollowing(@Path("id") String id, @Path("followeeId") String followeeId);

    @GET("api/tweeters/{id}/followings")
    Call<List<Following>> getAllFollowings(@Path("id") String id);

    //Get Following
    @GET("api/tweeters/{id}/followings")
    Call<Tweeter> getFollowing(@Path("id") String id);
}

package com.ictskills.mytweet.activities.tweeter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.ictskills.mytweet.R;
import com.ictskills.mytweet.activities.account.SetPreferences;
import com.ictskills.mytweet.activities.tweet.TweetFragment;
import com.ictskills.mytweet.activities.tweet.TweetPager;
import com.ictskills.mytweet.containers.AllTweetsPortfolio;
import com.ictskills.mytweet.containers.FollowPortfolio;
import com.ictskills.mytweet.containers.TweeterPortfolio;
import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Following;
import com.ictskills.mytweet.models.Tweet;
import com.ictskills.mytweet.models.Tweeter;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static android.app.PendingIntent.getActivity;
import static com.ictskills.android.helpers.IntentHelper.navigateUp;
import static com.ictskills.android.helpers.LogHelpers.info;

public class TweeterFragment extends Activity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private ListView listView;
    private TweeterTweetAdapter adapter;

    private MyTweetApp app;

    private ListView listview;
    private TweeterPortfolio tweeterPortfolio;
    private FollowPortfolio followPortfolio;
    private AllTweetsPortfolio allTweetsPortfolio;

    private ArrayList<Tweet> allTweets;
    private ArrayList<Tweet> tweeterTweets = new ArrayList<>();
    private ArrayList<Tweeter> followed;

    private Tweeter tweeter;
    private TextView tweeterName;
    private TextView tweeterEmail;
    private Button followButton;
    private CheckBox isfollowed;
    private Tweeter currentTweeter;


    public static final String EXTRA_TWEETER_ID = "mytweet.TWEETER_ID";
    private static final int SETTINGS_RESULT = 1;

    View fragment_tweeter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_tweeter);
        //setHasOptionsMenu(true);

        app = (MyTweetApp) getApplication();

        followPortfolio = app.followPortfolio;
        followed = followPortfolio.followed;

        tweeterPortfolio = app.tweeterPortfolio;

        allTweetsPortfolio = app.allTweetsPortfolio;
        allTweets = allTweetsPortfolio.allTweets;

        listView = (ListView) findViewById(R.id.tweeterTweetList);
        adapter = new TweeterTweetAdapter (this, tweeterTweets);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        getActionBar().setDisplayHomeAsUpEnabled(true);

        String twId = (String) getIntent().getExtras().getSerializable(EXTRA_TWEETER_ID);
        tweeter = tweeterPortfolio.getTweeter(twId);

        currentTweeter = app.currentTweeter;

        addListeners();
        updateControls(tweeter);
        checkFollowing(tweeter);
    }

    private void addListeners() {
        followButton = (Button) findViewById(R.id.followButton);
        tweeterName = (TextView) findViewById(R.id.tweeterName);
        tweeterEmail = (TextView) findViewById(R.id.tweeterEmail);
        isfollowed = (CheckBox) findViewById(R.id.isfollowed);

        followButton.setOnClickListener(this);
    }

    public void updateControls(Tweeter tweeter) {
        tweeterName.setText(tweeter.getName());
        tweeterEmail.setText(tweeter.email);
        isfollowed.setChecked(tweeter.followed);
    }

    public void getTweeterTweets() {
        Call<List<Tweet>> call = app.tweetService.getTweets(tweeter.id);
        call.enqueue(new Callback<List<Tweet>>() {
            public void onResponse(Response<List<Tweet>> response, Retrofit retrofit) {
                List<Tweet> list = response.body();
                info(this, "response list" + list);
                tweeterTweets.addAll(list);
                info(this, "tweeterTweets " + tweeterTweets);
                allTweets.addAll(tweeterTweets);
                info(this, "allTweets " + allTweets);
                //Toast.makeText(this, "Tweets retrieved" + response.body().size(), Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.followButton:
                if(followed.contains(tweeter)) {
                    info(this, "contains tweeter = true");
                    followButton.setText("Follow");
                    tweeter.followed = false;
                    isfollowed.setChecked(false);
                    info(this, "deleteRemoteFollowing");
                    deleteRemoteFollowing(app.currentTweeter.id, tweeter.id);
                    followPortfolio.deleteFollowing(tweeter);
                    followPortfolio.updateFollowings(followed);
                    for (Tweet tw : tweeterTweets)
                    {
                        allTweets.remove(tw);
                    }
                    allTweets.remove(tweeterTweets);
                    tweeterTweets.clear();
                    adapter.notifyDataSetChanged();
                }
                else
                {
                    info(this, "contains tweeter = false");
                    followPortfolio.addFollowing(tweeter);
                    followButton.setText("UnFollow");
                    tweeter.followed = true;
                    isfollowed.setChecked(true);
                    addRemoteFollowing(app.currentTweeter.id, tweeter.id);
                    info(this, "followPortfolio" + followPortfolio);
                    getTweeterTweets();
                    info(this, "getTweeterTweets " + tweeterTweets);
                    //allTweets.addAll(tweeterTweets);
                    info(this, "allTweets" + allTweets);
                    adapter.notifyDataSetChanged();
                }

                adapter.notifyDataSetChanged();
        }
    }

    private void addRemoteFollowing(String id, String tweeterId) {
        Call<Following> call = app.tweetService.createFollowing(id, tweeterId);
        call.enqueue(new Callback<Following>() {
            @Override
            public void onResponse(Response<Following> response, Retrofit retrofit) {
                Following returnedTweeter = response.body();

                // If returned following matches the following created and transmitted to the server then success.
                if (tweeter.id.equals(returnedTweeter.id)) {
                    //Toast.makeText(getActivity(), "Following created successfully", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(getActivity(), "Failed to create Following", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                //Toast.makeText(getActivity(), "Failed to create following due to unknown network issue", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteRemoteFollowing(String id, String tweeterId) {
        Call<Following> call = app.tweetService.deleteFollowing(id, tweeterId);
        call.enqueue(new Callback<Following>() {

            @Override
            public void onResponse(Response<Following> response, Retrofit retrofit) {
                //Toast.makeText(getActivity(), "Following deleted" + response.body(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                //Toast.makeText(getActivity(), "Failed to delete following due to unknown network issue", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean checkFollowing(Tweeter tweeter)
    {
        if(followed.contains(tweeter))
        {
            followButton.setText("UnFollow");
            tweeter.followed = true;
            isfollowed.setChecked(true);
            getTweeterTweets();
            return true;
        }
        else {
            followButton.setText("Follow");
            tweeter.followed = false;
            isfollowed.setChecked(false);
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tweet_list, menu);
        return true;
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        tweeter.followed = isChecked;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateUp(this);
                return true;

                case R.id.tweetersList:
                    Intent i = new Intent(this, TweeterList.class);
                    startActivity(i);
                    return true;

                case R.id.menuTweet:
                    Tweet tweet = new Tweet();
                    app.portfolio.addTweet(tweet);

                    i = new Intent(this, TweetPager.class);
                    i.putExtra(TweetFragment.EXTRA_TWEET_ID, tweet.id);
                    startActivityForResult(i, 0);
                    return true;

                case R.id.action_settings:
                    i = new Intent(this, SetPreferences.class);
                    startActivityForResult(i, SETTINGS_RESULT);
                    return true;

                default:
                    return super.onOptionsItemSelected(item);
            }
        }
}

class TweeterTweetAdapter extends ArrayAdapter<Tweet> {
    private Context context;
    public List<Tweet> tweeterTweets;

    public TweeterTweetAdapter(Context context, ArrayList<Tweet> tweeterTweets) {
        super(context, 0, tweeterTweets);
        this.context = context;
        this.tweeterTweets = tweeterTweets;
    }

    /*
        Method to create view of each tweet on list, loads each tweets' tweetText, ellipse, and
        date of tweet
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_tweet, null);
        }

        //Tweet tw = getItem(position);
        Tweet tw = tweeterTweets.get(position);
        //TextView tweeter = (TextView) convertView.findViewById(R.id.tweetListItem_tweeter);
        // tw.tweeterId = tweeter.id;
        // tweeter.setText(tw.tweeter.getName()); //.firstName.toString() + " " + tw.tweeter.lastName.toString());

        TextView tweetText = (TextView) convertView.findViewById(R.id.tweetListItem_tweetText);
        tweetText.setText(tw.tweetText);

        TextView tweetDots = (TextView) convertView.findViewById(R.id.tweetListItem_dots);
        tweetDots.setText("...");

        TextView dateTextView = (TextView) convertView.findViewById(R.id.tweetListItem_date);
        dateTextView.setText(tw.timeSince(tw));

        return convertView;
    }
}

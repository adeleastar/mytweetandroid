package com.ictskills.mytweet.activities.account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ictskills.mytweet.R;
import com.ictskills.mytweet.activities.tweet.TweetList;
import com.ictskills.mytweet.containers.AllTweetsPortfolio;
import com.ictskills.mytweet.containers.FollowPortfolio;
import com.ictskills.mytweet.containers.Portfolio;
import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Following;
import com.ictskills.mytweet.models.Tweet;
import com.ictskills.mytweet.models.Tweeter;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.ictskills.android.helpers.LogHelpers.info;

public class Login extends Activity

{
    private MyTweetApp app;
    private Portfolio portfolio;
    private FollowPortfolio followPortfolio;
    private AllTweetsPortfolio allTweetsPortfolio;
    private List<Following> followings = new ArrayList<>();
    private List<Tweet> tweets;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        app = (MyTweetApp) getApplication();
        portfolio = app.portfolio;
        followPortfolio = app.followPortfolio;
        allTweetsPortfolio = app.allTweetsPortfolio;
    }


    //press signin, check valid user, call methods to getFollowings, getTweets, load TweetList
    public void signinPressed(View view)
    {
        TextView email = (TextView) findViewById(R.id.loginEmail);
        TextView password = (TextView) findViewById(R.id.loginPassword);

        if (app.validTweeter(email.getText().toString(), password.getText().toString()))
        {
            info(this, "app.currentTweeter.id " + app.currentTweeter.id);
            getFollowings();
            getTweets();
            startActivity(new Intent(this, TweetList.class));
        }
        else
        {
            Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    //get tweets for current user, update portfolio.tweets with tweets
    public void getTweets()
    {
        Call<List<Tweet>> call = app.tweetService.getTweets(app.currentTweeter.id);
        call.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void onResponse(Response<List<Tweet>> response, Retrofit retrofit) {
                app.tweets.clear();
                app.tweets = response.body();
                portfolio.updateTweets(app.tweets);
                allTweetsPortfolio.addAllTweets(app.tweets);
                info(this, "app.portfolio " + app.portfolio.tweets);
                info(this, "allTweetsPortfolio " + allTweetsPortfolio);
            }

            @Override
            public void onFailure(Throwable t) {}
        });
    }

    // Get followings for currentTweeter, add to list of followings, call method get Followers
    public void getFollowings()
    {
        try{
        Call<List<Following>> call = app.tweetService.getAllFollowings(app.currentTweeter.id);
        call.enqueue(new Callback<List<Following>>() {
            @Override
            public void onResponse(Response<List<Following>> response, Retrofit retrofit) {
                followings = response.body();
                info(this, "followings " + followings);
                getFollowers();
            }

            @Override
            public void onFailure(Throwable t) {}
        });}
        catch (Exception e) {}
    }

    /* For each following in followings list get tweeter/followee and add to followPortfolio
        followed list, call method to getFollowersTweets
     */
    public void getFollowers()
    {
        try {
            for (Following follow : followings) {
                followPortfolio.addFollowing(follow.followee);
                info(this, "followPortfolio " + follow.followee.firstName);
            }
            getFollowersTweets();
        }
        catch (Exception e) {}
    }

    /* For tweeter in app.followPortfolio.followed list get tweeters tweets, add to
        app.tweeterTwPortfolio.tweeterTweets list
     */
    public void getFollowersTweets()
    {
        try{
        for (Tweeter t : app.followPortfolio.followed)
        {
                Call<List<Tweet>> call = app.tweetService.getTweets(t.id);
                call.enqueue(new Callback<List<Tweet>>() {
                @Override
                public void onResponse(Response<List<Tweet>> response, Retrofit retrofit) {
                    tweets = response.body();
                    allTweetsPortfolio.addAllTweets(tweets);
                    info(this, "getFollowersTweets " + app.tweets);
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }}
        catch (Exception e) {}
    }
}

package com.ictskills.mytweet.activities.tweet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ictskills.mytweet.R;
import com.ictskills.mytweet.activities.account.SetPreferences;
import com.ictskills.mytweet.activities.tweeter.TweeterList;
import com.ictskills.mytweet.containers.AllTweetsPortfolio;
import com.ictskills.mytweet.containers.FollowPortfolio;
import com.ictskills.mytweet.containers.Portfolio;
import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Tweet;
import com.ictskills.mytweet.models.Tweeter;
import com.ictskills.mytweet.utils.TweetDateComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.ictskills.android.helpers.LogHelpers.info;

public class TweetListFragment extends ListFragment implements AdapterView.OnItemClickListener, AbsListView.MultiChoiceModeListener {

    private TweetAdapter adapter;
    private ListView listView;

    private MyTweetApp app;

    private ArrayList<Tweet> tweetHolder = new ArrayList<Tweet>();
    private ArrayList<Tweet> allTweets;
    private ArrayList<Tweet> tweets;
    private ArrayList<Tweeter> followed;

    private Portfolio portfolio;
    private FollowPortfolio followPortfolio;
    private IntentFilter intentFilter;
    private Tweeter currentTweeter;
    private AllTweetsPortfolio allTweetsPortfolio;

    private static final int SETTINGS_RESULT = 1;
    public static final String BROADCAST_ACTION = "com.ictskills.mytweet.activities.tweet.TweetListFragment";

    /*
        Method to load items on startup, loads MyTweetApp, portfolio containing list of tweets, and
        tweet ID, sets options menu, loads tweet adapter to allow listing of tweets on page
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        app = (MyTweetApp) getActivity().getApplication();

        currentTweeter = app.currentTweeter;

        portfolio = app.portfolio;
        tweets = portfolio.tweets;

        followPortfolio = app.followPortfolio;
        followed = followPortfolio.followed;

        allTweetsPortfolio = app.allTweetsPortfolio;

        info(this, "allTweetsPortfolio " + allTweetsPortfolio.allTweets.toString());

        allTweets = allTweetsPortfolio.allTweets;

        intentFilter = new IntentFilter(BROADCAST_ACTION);

        registerBroadcastReceiver(intentFilter);

        adapter = new TweetAdapter(getActivity(), allTweets);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    /*
        Method to load layout view of tweetList with list of tweets, sets choiceMode to allow
        selection of tweets on screen for deleting
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.tweet_list, menu);
    }

    /*
        Method to set actions on menu option items selected: menuTweet creates new tweet, adds to
        portfolio list of tweets, creates new intent to load TweetPager and tweet screen with id of
        said tweet; action_settings creates intent to start SetPreferences and load settings
        screen; clear calls deleteAllTweets() in portfolio method to clear screen of all tweets and
        notifies list adapter to change view
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tweetersList:
                Intent i = new Intent(getActivity(), TweeterList.class);
                startActivity(i);
                return true;

            case R.id.menuTweet:
                Tweet tweet = new Tweet();
                portfolio.addTweet(tweet);

                i = new Intent(getActivity(), TweetPager.class);
                i.putExtra(TweetFragment.EXTRA_TWEET_ID, tweet.id);
                startActivityForResult(i, 0);
                return true;

            case R.id.action_refresh:
                refreshList();
                return true;

            case R.id.my_tweets:
                allTweets = portfolio.tweets;
                adapter.notifyDataSetChanged();
                return true;

            case R.id.all_tweets:
                allTweets = allTweetsPortfolio.allTweets;
                adapter.notifyDataSetChanged();
                //refreshList();
                return true;

            case R.id.action_settings:
                i = new Intent(getActivity(), SetPreferences.class);
                startActivityForResult(i, SETTINGS_RESULT);
                return true;

            case R.id.clear:
                deleteAllRemoteTweets();
                allTweetsPortfolio.deleteAllTweets();
                portfolio.deleteAllTweets();
                app.tweets.clear();
                adapter.notifyDataSetChanged();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }

    /*
        Method to enable launching of correct tweet on clicking, uses TweetAdapter to find ID of
        tweet by position, and launch fragment with relevant tweet info
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Tweet tw = ((TweetAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), TweetPager.class);
        i.putExtra(TweetViewFragment.EXTRA_TWEET_ID, tw.id);
        startActivityForResult(i, 0);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.tweetlistcontext, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    /*  Method to delete tweet on selection, loads delete menu on long hold, removes tweet from
        list, notifies adapter to refresh list of tweets
     */
    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deleteTweet:
                for (int i = adapter.getCount() - 1; i >= 0; i--) {
                    if (listView.isItemChecked(i)) {
                        Tweet tw = adapter.getItem(i);
                        deleteRemoteTweet(app.currentTweeter.id, tw.id);
                        portfolio.deleteTweet(tw);
                        allTweetsPortfolio.deleteTweet(tw);
                    }
                }
                mode.finish();
                adapter.notifyDataSetChanged();
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        //((TweetAdapter) getListAdapter()).notifyDataSetChanged();

    }

    public void refreshList() {
        info(this, "refresh List called");
        allTweets.clear();
        portfolio.deleteAllTweets();
        adapter.notifyDataSetChanged();
        info(this, "allTweets " + allTweets);
        app.tweets.clear();
        info(this, "app.tweets " + app.tweets);
        getTweetersTweets();

    }

    private void deleteRemoteTweet(String id, String tweetId) {
        Call<Tweet> call = app.tweetService.deleteTweet(id, tweetId);
        call.enqueue(new Callback<Tweet>() {

            @Override
            public void onResponse(Response<Tweet> response, Retrofit retrofit) {
                Toast.makeText(getActivity(), "Tweet deleted: " + response.body(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(), "Failed to delete Tweet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteAllRemoteTweets() {
        Call<String> call = app.tweetService.deleteAllTweets();
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Response<String> response, Retrofit retrofit) {
                Toast.makeText(getActivity(), "All Tweets deleted: " + response.body(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(), "Failed to delete all tweets", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getTweets() {
        Call<List<Tweet>> call = app.tweetService.getTweets(currentTweeter.id);
        call.enqueue(new Callback<List<Tweet>>() {
            public void onResponse(Response<List<Tweet>> response, Retrofit retrofit) {
                app.tweets.clear();

                app.tweets = response.body();

                portfolio.updateTweets(app.tweets);
                info(this, "portfolio.tweets " + portfolio.tweets);

                tweetHolder.addAll(portfolio.tweets);
                info(this, "getTweets" + tweetHolder);

                Toast.makeText(getActivity(), "Tweets retrieved" + response.body().size(), Toast.LENGTH_SHORT).show();

                allTweets.addAll(tweetHolder);

                info(this, "allTweets " + allTweets);

                info(this, "allTweetsPortfolio " + allTweetsPortfolio.allTweets);

                Collections.sort(allTweets, new TweetDateComparator());

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

    public void getTweetersTweets() {
        if (followed != null) {
            for (Tweeter tw : followed) {
                info(this, "followed " + tw);
                Call<List<Tweet>> call = app.tweetService.getTweets(tw.id);
                call.enqueue(new Callback<List<Tweet>>() {
                    public void onResponse(Response<List<Tweet>> response, Retrofit retrofit) {
                        app.tweets.clear();

                        app.tweets = response.body();

                        info(this, "app.tweets" + app.tweets);

                        tweetHolder.clear();

                        tweetHolder.addAll(app.tweets);

                        info(this, "getTweetersTweets" + tweetHolder);
                        portfolio.deleteAllTweets();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                    }
                });
            }
        }
        getTweets();
    }

    private void registerBroadcastReceiver(IntentFilter intentFilter) {
        ResponseReceiver responseReceiver = new ResponseReceiver();
        // Registers the ResponseReceiver and its intent filters
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(responseReceiver, intentFilter);
    }

    //Broadcast receiver for receiving status updates from the IntentService
    private class ResponseReceiver extends BroadcastReceiver {
        //private void ResponseReceiver() {}
        // Called when the BroadcastReceiver gets an Intent it's registered to receive
        @Override
        public void onReceive(Context context, Intent intent) {
            adapter.allTweets = allTweets;
            adapter.notifyDataSetChanged();
        }
    }
}

class TweetAdapter extends ArrayAdapter<Tweet> {
    private Context context;
    public List<Tweet> allTweets;

    public TweetAdapter(Context context, ArrayList<Tweet> allTweets) {
        super(context, 0, allTweets);
        this.context = context;
        this.allTweets = allTweets;
    }

    /*
        Method to create view of each tweet on list, loads each tweets' tweetText, ellipse, and
        date of tweet
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_tweet, null);
        }

        Tweet tw = allTweets.get(position);

        TextView tweetText = (TextView) convertView.findViewById(R.id.tweetListItem_tweetText);
        tweetText.setText(tw.tweeterTweetText);

        TextView tweetDots = (TextView) convertView.findViewById(R.id.tweetListItem_dots);
        tweetDots.setText("...");

        TextView dateTextView = (TextView) convertView.findViewById(R.id.tweetListItem_date);
        dateTextView.setText(tw.timeSince(tw));

        return convertView;
    }
}
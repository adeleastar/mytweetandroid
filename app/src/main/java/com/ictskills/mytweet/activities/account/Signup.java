package com.ictskills.mytweet.activities.account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ictskills.mytweet.R;
import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Tweeter;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class Signup extends Activity implements Callback<Tweeter>
{
    private MyTweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        app = (MyTweetApp) getApplication();
    }

    public void registerPressed (View view)
    {
        TextView firstName = (TextView)  findViewById(R.id.firstName);
        TextView lastName  = (TextView)  findViewById(R.id.lastName);
        TextView email     = (TextView)  findViewById(R.id.Email);
        TextView password  = (TextView)  findViewById(R.id.Password);

        Tweeter tweeter = new Tweeter(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password.getText().toString());

        Call<Tweeter> call = app.tweetService.createTweeter(tweeter);
        call.enqueue(this);
}

    @Override
    public void onResponse(Response<Tweeter> response, Retrofit retrofit) {
        app.tweeters.add(response.body());
        startActivity(new Intent(this, Login.class));
    }

    @Override
    public void onFailure(Throwable t)
    {
        app.tweetServiceAvailable = false;
        Toast toast = Toast.makeText(this, "MyTweet Service Unavailable. Please Try Again Later", Toast.LENGTH_SHORT);
        toast.show();
        startActivity(new Intent(this, Welcome.class));
    }
 }


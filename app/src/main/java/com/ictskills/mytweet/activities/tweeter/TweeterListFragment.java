
package com.ictskills.mytweet.activities.tweeter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ictskills.mytweet.R;
import com.ictskills.mytweet.activities.account.SetPreferences;
import com.ictskills.mytweet.activities.tweet.TweetFragment;
import com.ictskills.mytweet.activities.tweet.TweetList;
import com.ictskills.mytweet.activities.tweet.TweetPager;
import com.ictskills.mytweet.containers.FollowPortfolio;
import com.ictskills.mytweet.containers.Portfolio;
import com.ictskills.mytweet.containers.TweeterPortfolio;
import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Tweet;
import com.ictskills.mytweet.models.Tweeter;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class TweeterListFragment extends ListFragment implements AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener, View.OnClickListener, AbsListView.MultiChoiceModeListener {

    private TweeterAdapter adapter;

    private ListView listView;

    public MyTweetApp app;

    private ArrayList<Tweeter> tweeters;
    public ArrayList<Tweeter> followed;
    private TweeterPortfolio tweeterPortfolio;
    private Portfolio portfolio;
    private FollowPortfolio followPortfolio;
    private Tweeter tweeter;
    private Tweeter currentTweeter;
    private Button followButton;


    private static final int SETTINGS_RESULT = 1;

/* Method to load items on startup, loads MyTweetApp, portfolio containing list of tweets, and
     tweet ID, sets options menu, loads tweet adapter to allow listing of tweets on page
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //getActivity().setTitle(R.string.app_name);

        app = (MyTweetApp) getActivity().getApplication();

        tweeterPortfolio = app.tweeterPortfolio;
        tweeters = tweeterPortfolio.tweeters;
        tweeters.remove(app.currentTweeter);

        portfolio = app.portfolio;


        followPortfolio = app.followPortfolio;
        followed = followPortfolio.followed;

        currentTweeter = app.currentTweeter;

        adapter = new TweeterAdapter(getActivity(), tweeters);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();
        checkFollowing();
    }

    /*
         Method to load layout view of tweetList with list of tweets, sets choiceMode to allow
        selection of tweets on screen for deleting
    */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        listView = (ListView) v.findViewById(android.R.id.list);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.tweeter_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tweetList:
                Intent i = new Intent(getActivity(), TweetList.class);
                startActivity(i);
                return true;

            case R.id.menuTweet:
                Tweet tweet = new Tweet();

                portfolio.addTweet(tweet);

                i = new Intent(getActivity(), TweetPager.class);
                i.putExtra(TweetFragment.EXTRA_TWEET_ID, tweet.id);
                startActivityForResult(i, 0);
                return true;

            case R.id.action_refresh:
                refreshList();
                return true;

            case R.id.my_tweets:
                getTweets();
                return true;

            case R.id.all_tweets:
                refreshList();
                return true;

            case R.id.action_settings:
                i = new Intent(getActivity(), SetPreferences.class);
                startActivityForResult(i, SETTINGS_RESULT);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean checkFollowing() {
            for (Tweeter tweeter : tweeters) {
                if (followed.contains(tweeter)) {
                    tweeter.followed = true;
                    return true;
                }
            }
        return false;
    }

    public void getTweets() {
        Call<List<Tweet>> call = app.tweetService.getTweets(currentTweeter.id);
        call.enqueue(new Callback<List<Tweet>>() {
            public void onResponse(Response<List<Tweet>> response, Retrofit retrofit) {
                List<Tweet> list = response.body();
                portfolio.updateTweets(list);
                adapter.notifyDataSetChanged();
                Toast.makeText(getActivity(), "Tweets retrieved" + response.body().size(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

/*
Method to enable launching of correct tweeter on clicking, uses TweeterAdapter to find ID of
tweeter by position, and launch fragment with relevant tweeter info
*/

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Tweeter tw = ((TweeterAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), TweeterFragment.class);
        i.putExtra(TweeterFragment.EXTRA_TWEETER_ID, tw.id);
        startActivityForResult(i, 0);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        /*Tweeter tw = adapter.getItem(position);
        IntentHelper.startActivityWithData(getActivity(), TweeterPager.class, "TWEETER_ID", tw.id);*/
    }


    @Override
    public void onResume() {
        super.onResume();
        ((TweeterAdapter) getListAdapter()).notifyDataSetChanged();
    }

    public void refreshList() {
        Call<List<Tweeter>> call = app.tweetService.getAllTweeters();
        call.enqueue(new Callback<List<Tweeter>>() {

            @Override
            public void onResponse(Response<List<Tweeter>> response, Retrofit retrofit) {
                app.tweeters = response.body();
                Toast.makeText(getActivity(), "Retrieved " + app.tweeters.size() + " tweeters", Toast.LENGTH_LONG).show();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getActivity(), "Failed to retrieve tweet list", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    }

    @Override
    public void onClick(View v) {
    }

    public void getTweeter(String id) {
        Call<Tweeter> call = app.tweetService.getTweeter(id);
        call.enqueue(new Callback<Tweeter>() {
            @Override
            public void onResponse(Response<Tweeter> response, Retrofit retrofit) {
                Tweeter tweeter = response.body();
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });

    }

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
    }
}

class TweeterAdapter extends ArrayAdapter<Tweeter> {

    private Context context;

    public TweeterAdapter(Context context, ArrayList<Tweeter> tweeters) {
        super(context, 0, tweeters);
        this.context = context;
    }

    /*
    Method to create view of each tweet on list, loads each tweets' tweetText, ellipse, and
    date of tweet
    */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_tweeter, null);
        }
                Tweeter tw = getItem(position);

                TextView tweeterName = (TextView) convertView.findViewById(R.id.tweeterListItem_tweeter);
                tweeterName.setText(tw.getName());

                TextView tweeterEmail = (TextView) convertView.findViewById(R.id.tweeterListItem_email);
                tweeterEmail.setText(tw.email);

                CheckBox isFollowed = (CheckBox) convertView.findViewById(R.id.tweeterListItem_isfollowed);
                isFollowed.setChecked(tw.followed);

        return convertView;
    }
}


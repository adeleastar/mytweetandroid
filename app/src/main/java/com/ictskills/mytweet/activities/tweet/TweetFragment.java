package com.ictskills.mytweet.activities.tweet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ictskills.android.helpers.ContactHelper;
import com.ictskills.mytweet.R;
import com.ictskills.mytweet.containers.Portfolio;
import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Tweet;

import java.util.HashMap;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.ictskills.android.helpers.IntentHelper.sendEmail;

public class TweetFragment extends Fragment implements TextWatcher, CompoundButton
        .OnCheckedChangeListener, View.OnClickListener, Callback<Tweet> {

    public static final String EXTRA_TWEET_ID = "mytweet.TWEET_ID";

    private static final int REQUEST_CONTACT = 1;

    private TextView charCountText;

    private EditText tweetText;

    private Button tweetButton;
    private Button selectContact;
    private Button emailContact;

    private Tweet tweet;
    private Portfolio portfolio;

    private MyTweetApp app;

    /*
        Method to load items on startup, loads MyTweetApp, portfolio containing list of tweets, and
        tweet ID, allows loading of individual tweet from portfolio of tweets
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        String twId = (String) getArguments().getSerializable(EXTRA_TWEET_ID);
        app = (MyTweetApp) getActivity().getApplication();
        portfolio = app.portfolio;
        tweet = portfolio.getTweet(twId);
    }

    /*
        Method to load layout view of tweet with actionbar with up button enabled
        sets title of view. Calls methods to add listeners and update controls.
        Returns view v
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_tweet, parent, false);
        getActivity().getActionBar().setTitle(R.string.tweetText);
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        addListeners(v);
        updateControls(tweet);

        return v;
    }

    /*
        Method to assign tweet variables to the corresponding widgets on layout page.
        Adds listeners to widget objects
     */
    private void addListeners(View v) {
        charCountText = (TextView) v.findViewById(R.id.charCount);
        tweetText = (EditText) v.findViewById(R.id.tweetText);

        tweetButton = (Button) v.findViewById(R.id.tweetButton);
        selectContact = (Button) v.findViewById(R.id.selectContact);
        emailContact = (Button) v.findViewById(R.id.emailContact);

        tweetText.addTextChangedListener(this);
        tweetButton.setOnClickListener(this);
        selectContact.setOnClickListener(this);
        emailContact.setOnClickListener(this);
    }

    /*
        Method to update controls on loading tweet; sets charCount widget to value
        held in tweets charCount variable, sets tweetText widget to contain text held
        in tweets tweetText variable.
     */
    public void updateControls(Tweet tweet) {
        charCountText.setText(String.valueOf(tweet.charCount));
        tweetText.setText(tweet.tweetText);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
        Method to start activities when tweetButton, selectContact, and emailContact buttons clicked.
        When button clicked: tweetButton option checks if tweetText variable is empty and returns differing
        messages if empty of full; selectContact starts new method onActivityResult to add tweet
        contacts email to widget; emailContact starts method to open email app and send with tweet
        subject, and text
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tweetButton:
                if (tweet.tweetText.isEmpty()) {
                    Toast toast = Toast.makeText(getActivity(), "Tweet is Empty!", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    tweet.buttonClicked = true;
                    //tweet.id = EXTRA_TWEET_ID
                    tweet.tweeterTweetText = app.currentTweeter.firstName + " says " + tweet.tweetText;
                    Toast toast = Toast.makeText(getActivity(), "Tweet Sent!", Toast.LENGTH_SHORT);
                    toast.show();

                    // API call after tweet fully formed
                    Call<Tweet> call = app.tweetService.createTweet(app.currentTweeter.id, tweet);
                    call.enqueue(this);

                    //Intent i = startActivity (new Intent(this, TweetList.class);
                    Intent i = new Intent(getActivity(), TweetList.class);
                    startActivity(i);
                }
                break;

            case R.id.selectContact:
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                if (tweet.tweetContact != null) {
                    selectContact.setText(tweet.tweetContact);
                }
                break;

            case R.id.emailContact:
                sendEmail(getActivity(), tweet.tweetContact, getString(R.string.tweetSubject), tweet.getTweet());
                break;
        }
    }

    /*
        Helper method, when onClick selectContact selected it finds email and name
        of contact. Populates tweetContact variable with email address to help
        with emailing contact, and inserts contact name in selectContact widget
    */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        } else if (requestCode == REQUEST_CONTACT) {
            String email = ContactHelper.getEmail(getActivity(), data);
            String name = ContactHelper.getDisplayName(getActivity(), data);
            tweet.tweetContact = email;
            selectContact.setText(name);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    /*  Method to reduce value held by charCount variable by the characters length and then set
        charCountText widget to said value
    */
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        charCountText.setText(String.valueOf(tweet.charCount - s.length()));
    }

    // Method to set tweetText widget to contain text entered
    @Override
    public void afterTextChanged(Editable s) {
        tweet.setTweetText(s.toString());
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    }

    @Override
    public void onPause() {
        super.onPause();
        portfolio.saveTweets();
    }

    @Override
    public void onResponse(Response<Tweet> response, Retrofit retrofit) {
        Tweet returnedTweet = response.body();
        // If returned tweet matches the tweet we created and transmitted to the server then success.
        //compare to 3 objects not date
        if (tweet.tweetText.equals(returnedTweet.tweetText) && (tweet.id.equals(returnedTweet.id)))
        {
            Toast.makeText(getActivity(), "Tweet created successfully", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(), "Failed to create Tweet", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        Toast.makeText(getActivity(), "Failed to create tweet due to unknown network issue", Toast.LENGTH_LONG).show();
    }
}

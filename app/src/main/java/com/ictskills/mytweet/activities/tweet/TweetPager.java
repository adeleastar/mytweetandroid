package com.ictskills.mytweet.activities.tweet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.ictskills.mytweet.R;
import com.ictskills.mytweet.containers.AllTweetsPortfolio;
import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.containers.Portfolio;
import com.ictskills.mytweet.models.Tweet;

import java.util.ArrayList;

import static com.ictskills.android.helpers.IntentHelper.navigateUp;

public class TweetPager extends FragmentActivity implements ViewPager.OnPageChangeListener {

    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;

    private ArrayList<Tweet> allTweets;
    private AllTweetsPortfolio allTweetsPortfolio;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewPager = new ViewPager(this);
        viewPager.setId(R.id.viewPager);
        setContentView(viewPager);

        setTweetList();

        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), allTweets);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);

        setCurrentItem();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    /*
        Method to set action on up button clicked, finds ID of current tweet, if tweetText is empty
        or buttonClick variable is set to false the tweet is deleted from portfolio
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                String twId = (String) getIntent().getSerializableExtra(TweetFragment.EXTRA_TWEET_ID);
                Tweet tweet = allTweetsPortfolio.getTweet(twId);
                if (tweet.tweetText.isEmpty() || !tweet.buttonClicked) {
                    allTweetsPortfolio.deleteTweet(tweet);
                }
                navigateUp(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Method to set list of tweets from portfolio array of tweets
    private void setTweetList() {
        MyTweetApp app = (MyTweetApp) getApplication();
        allTweetsPortfolio = app.allTweetsPortfolio;
        allTweets = allTweetsPortfolio.allTweets;
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

   // Method to set tweet details relevant to tweet ID
    private void setCurrentItem() {
        String tw = (String) getIntent().getSerializableExtra(TweetViewFragment.EXTRA_TWEET_ID);
        for (int i = 0; i < allTweets.size(); i++) {
            if (allTweets.get(i).id.equals(tw)) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }

    class PagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<Tweet> allTweets;

        public PagerAdapter(FragmentManager fm, ArrayList<Tweet> allTweets) {
            super(fm);
            this.allTweets = allTweets;
        }

        @Override
        public int getCount() {
            return allTweets.size();
        }

        @Override
        public Fragment getItem(int pos) {
            Tweet tweet = allTweets.get(pos);
            Bundle args = new Bundle();
            args.putSerializable(TweetViewFragment.EXTRA_TWEET_ID, tweet.id);
            TweetViewFragment fragment = new TweetViewFragment();
            fragment.setArguments(args);
            return fragment;
        }
    }
}

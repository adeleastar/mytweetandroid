package com.ictskills.mytweet.activities.account;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.widget.EditText;

import com.ictskills.mytweet.R;

public class Settings extends FragmentActivity {
    private EditText tweetUsername, tweetPassword, tweetUpdateFrequency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_settings);
        getActionBar().setDisplayHomeAsUpEnabled(true);

    }

    // Method to set preference manager to correspond to settings screen data
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        tweetUsername.setText(sharedPrefs.getString("tweetUserName", "NOUSERNAME"));
        tweetPassword.setText(sharedPrefs.getString("tweetPassword", "NOPASSWORD"));
        tweetUpdateFrequency.setText(sharedPrefs.getString("tweetUpdateFrequency", "NOUPDATE"));
    }
}
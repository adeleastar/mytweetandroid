/*
package com.ictskills.mytweet.activities.tweeter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.ictskills.mytweet.R;
import com.ictskills.mytweet.containers.TweeterPortfolio;
import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Tweeter;

import java.util.ArrayList;

import static com.ictskills.android.helpers.IntentHelper.navigateUp;


public class TweeterPager extends FragmentActivity implements ViewPager.OnPageChangeListener {

    private ViewPager viewPager;

    private ArrayList<Tweeter> tweeters;
    private TweeterPortfolio tp;
    private PagerAdapter pagerAdapter;
    private MyTweetApp app;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MyTweetApp) getApplication();

        viewPager = new ViewPager(this);
        viewPager.setId(R.id.viewPager);
        setContentView(viewPager);

        setTweeterList();

        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tweeters);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);

        setCurrentItem();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    */
/*
        Method to set action on up button clicked, finds ID of current tweet, if tweetText is empty
        or buttonClick variable is set to false the tweet is deleted from portfolio
     *//*

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateUp(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Method to set list of tweeters from portfolio array of tweeters
    private void setTweeterList() {
        tp = app.tweeterPortfolio;
        tweeters = tp.tweeters;
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        Tweeter tweeter = tweeters.get(arg0);
        if (tweeter.firstName != null)
        {
            setTitle(tweeter.getName());
        }
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    // Method to set tweet details relevant to tweet ID
    private void setCurrentItem() {
        String tweeter = (String) getIntent().getSerializableExtra(TweeterFragment.EXTRA_TWEETER_ID);
        for (int i = 0; i < tweeters.size(); i++) {
            if (tweeters.get(i).id.equals(tweeter)) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }

    class PagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<Tweeter> tweeters;

        public PagerAdapter(FragmentManager fm, ArrayList<Tweeter> tweeters) {
            super(fm);
            this.tweeters = tweeters;
        }

        @Override
        public int getCount() {
            return tweeters.size();
        }

        @Override
        public Fragment getItem(int pos) {
            Tweeter tweeter = tweeters.get(pos);
            Bundle args = new Bundle();
            args.putSerializable(TweeterFragment.EXTRA_TWEETER_ID, tweeter.id);
            TweeterFragment fragmentactivity = new TweeterFragment();
            fragment.setArguments(args);
            return fragmentactivity;
        }
    }
}
*/

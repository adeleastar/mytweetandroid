package com.ictskills.mytweet.activities.account;

import android.app.Activity;
import android.os.Bundle;

public class SetPreferences extends Activity {

    // On create method to start fragment for settings screen
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsPreferences()).commit();
    }
}

package com.ictskills.mytweet.activities.account;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ictskills.mytweet.R;
import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Tweeter;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.ictskills.android.helpers.LogHelpers.info;

public class Welcome extends Activity implements Callback<List<Tweeter>>
{
    private MyTweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        app = (MyTweetApp) getApplication();
    }


    @Override
    public void onResume()
    {
        super.onResume();
        app.currentTweeter = null;
        Call<List<Tweeter>> call = app.tweetService.getAllTweeters();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Response<List<Tweeter>> response, Retrofit retrofit) {
        serviceAvailableMessage();
        app.tweeters = response.body();
        info(this, "app.tweeters" + app.tweeters);

        app.tweeterPortfolio.updateTweeters(app.tweeters);
        Toast.makeText(this, "Retrieved " + app.tweeters.size() + " tweeters", Toast.LENGTH_LONG).show();

        app.tweetServiceAvailable = true;
    }

    @Override
    public void onFailure(Throwable t)
    {
        app.tweetServiceAvailable = false;
        serviceUnavailableMessage();
        Toast.makeText(this, "Failed to retrieve tweeters", Toast.LENGTH_LONG).show();
    }

    public void loginPressed (View view)
    {
        if (app.tweetServiceAvailable)
        {
            startActivity(new Intent(this, Login.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    public void signupPressed (View view)
    {
        if (app.tweetServiceAvailable)
        {
            startActivity(new Intent(this, Signup.class));
        }
        else
        {
            serviceUnavailableMessage();
        }
    }

    void serviceUnavailableMessage()
    {
        Toast toast = Toast.makeText(this, "Tweet Service Unavailable. Please Try Again Later", Toast.LENGTH_SHORT);
        toast.show();
    }

    void serviceAvailableMessage()
    {
        Toast toast = Toast.makeText(this, "Tweet Service Contacted Successfully", Toast.LENGTH_SHORT);
        toast.show();
    }
}

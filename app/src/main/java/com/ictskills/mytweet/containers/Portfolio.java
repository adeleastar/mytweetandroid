package com.ictskills.mytweet.containers;

import android.util.Log;

import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import static com.ictskills.android.helpers.LogHelpers.info;

public class Portfolio {
    public ArrayList<Tweet> tweets = new ArrayList<>();

    public MyTweetApp app;

    private PortfolioSerializer serializer;

    public Portfolio(PortfolioSerializer serializer) {
        this.serializer = serializer;
        try {
            tweets = serializer.loadTweets();
        } catch (Exception e) {
            info(this, "Error loading tweets: " + e.getMessage());
        }
    }

    public boolean saveTweets() {
        try {
            serializer.saveTweets(tweets);
            info(this, "Tweets saved to file");
            return true;
        } catch (Exception e) {
            info(this, "Error saving Tweets: " + e.getMessage());
            return false;
        }
    }

    public void addTweet(Tweet tweet) {
        tweets.add(tweet);
    }

    public void addAllTweets(List<Tweet> list){
        tweets.addAll(list);
    }

    public Tweet getTweet(String id) {

        Log.i(this.getClass().getSimpleName(), "String parameter id: " + id);

        for (Tweet tw : tweets) {
            if (id.equals(tw.id)) {
                return tw;
            }
        }
        info(this, "failed to find Tweet, returning first element array to avoid crash");
        return null;
    }

    public void deleteTweet(Tweet t) {
        tweets.remove(t);
    }

    public void deleteTweets(List<Tweet> tweets) {
        for (Tweet tw : tweets)
        {
            tweets.remove(tw);
        }
    }

    public void deleteAllTweets() {
        tweets.removeAll(tweets);
    }

    public void updateTweets(List<Tweet> list) {
            tweets.clear();
            tweets.addAll(list);
            saveTweets();
    }
}

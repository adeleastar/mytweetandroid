package com.ictskills.mytweet.containers;

import android.util.Log;

import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Tweeter;

import java.util.ArrayList;
import java.util.List;

import static com.ictskills.android.helpers.LogHelpers.info;

public class TweeterPortfolio {
    public ArrayList<Tweeter> tweeters = new ArrayList<>();

    public MyTweetApp app;

    private TweeterPortfolioSerializer serializer;;

    public TweeterPortfolio(TweeterPortfolioSerializer serializer) {
        this.serializer = serializer;
        try {
            tweeters = serializer.loadTweeters();
        } catch (Exception e) {
            info(this, "Error loading tweeters: " + e.getMessage());
        }
    }

    public boolean saveTweeters() {
        try {
            serializer.saveTweeters(tweeters);
            info(this, "Tweets saved to file");
            return true;
        } catch (Exception e) {
            info(this, "Error saving Tweeters: " + e.getMessage());
            return false;
        }
    }

    public void addTweeter(Tweeter tweeter) {
        tweeters.add(tweeter);
    }


    public Tweeter getTweeter(String id) {

        Log.i(this.getClass().getSimpleName(), "String parameter id: " + id);

        for (Tweeter tw : tweeters) {
            if (id.equals(tw.id)) {
                return tw;
            }
        }
        info(this, "failed to find Tweeter, returning first element array to avoid crash");
        return null;
    }

    public void deleteTweeter(Tweeter t) {
        tweeters.remove(t);
    }

    public void deleteAllTweeters() {
        tweeters.removeAll(tweeters);
    }

    public void updateTweeters(List<Tweeter> list)
    {
        if (tweeters != null) {
                tweeters.clear();
                tweeters.addAll(list);
                saveTweeters();
            }
    }
}

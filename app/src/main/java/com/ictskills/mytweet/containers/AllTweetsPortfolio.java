package com.ictskills.mytweet.containers;

import android.util.Log;

import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import static com.ictskills.android.helpers.LogHelpers.info;

public class AllTweetsPortfolio {
    public ArrayList<Tweet> allTweets = new ArrayList<>();

    public MyTweetApp app;

    private AllTweetsPortfolioSerializer serializer;

    public AllTweetsPortfolio(AllTweetsPortfolioSerializer serializer) {
        this.serializer = serializer;
        try {
            allTweets = serializer.loadTweets();
        } catch (Exception e) {
            info(this, "Error loading tweets: " + e.getMessage());
        }
    }

    public boolean saveAllTweets() {
        try {
            serializer.saveTweets(allTweets);
            info(this, "Tweets saved to file");
            return true;
        } catch (Exception e) {
            info(this, "Error saving Tweets: " + e.getMessage());
            return false;
        }
    }

    public void addTweet(Tweet tweet) {
        allTweets.add(tweet);
    }

    public void addAllTweets(List<Tweet> list){
        allTweets.addAll(list);
    }

    public Tweet getTweet(String id) {

        Log.i(this.getClass().getSimpleName(), "String parameter id: " + id);

        for (Tweet tw : allTweets) {
            if (id.equals(tw.id)) {
                return tw;
            }
        }
        info(this, "failed to find Tweet, returning first element array to avoid crash");
        return null;
    }

    public void deleteTweet(Tweet t) {
        allTweets.remove(t);
    }

    public void deleteAllTweets() {
        allTweets.removeAll(allTweets);
    }

    public void deleteTweeterTweets(ArrayList<Tweet> tweeterTweets)
    {
        allTweets.remove(tweeterTweets);

    }
    public void updateTweets(List<Tweet> list) {
        allTweets.clear();
        allTweets.addAll(list);
        saveAllTweets();
    }

    public ArrayList<Tweet> getAllTweets() {
        allTweets.clear();
        allTweets.addAll(app.portfolio.tweets);
        saveAllTweets();
        return allTweets;
    }
}

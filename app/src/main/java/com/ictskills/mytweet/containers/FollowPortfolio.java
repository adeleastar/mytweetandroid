package com.ictskills.mytweet.containers;

import com.ictskills.mytweet.main.MyTweetApp;
import com.ictskills.mytweet.models.Following;
import com.ictskills.mytweet.models.Tweeter;

import java.util.ArrayList;
import java.util.List;

import static com.ictskills.android.helpers.LogHelpers.info;

public class FollowPortfolio {

    public ArrayList<Tweeter> followed = new ArrayList<>();

    public MyTweetApp app;

    private FollowPortfolioSerializer serializer;

    public FollowPortfolio(FollowPortfolioSerializer serializer) {
        this.serializer = serializer;
        try {
            followed = serializer.loadFollowings();
        } catch (Exception e) {
            info(this, "Error loading tweeters: " + e.getMessage());
        }
    }

    public boolean saveFollowings() {
        try {
            serializer.saveFollowings(followed);
            info(this, "Followings saved to file");
            return true;
        } catch (Exception e) {
            info(this, "Error saving Followings: " + e.getMessage());
            return false;
        }
    }

    public void addFollowing(Tweeter t) {
        followed.add(t);
    }

    public void deleteFollowing(Tweeter t) {
        followed.remove(t);
    }

    public void deleteAllFollowings() {
        followed.removeAll(followed);
    }

    public void updateFollowings(List<Tweeter> list)
    {
        followed.clear();
        followed.addAll(list);
        saveFollowings();
    }
}


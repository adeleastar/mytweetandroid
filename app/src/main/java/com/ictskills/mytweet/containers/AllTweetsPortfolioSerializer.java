package com.ictskills.mytweet.containers;

import android.content.Context;

import com.ictskills.mytweet.models.Tweet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by Aine on 03/01/2016.
 */
public class AllTweetsPortfolioSerializer {

    private Context mContext;
    private String mFilename;

    public AllTweetsPortfolioSerializer(Context c, String f) {
        mContext = c;
        mFilename = f;
    }

    public void saveTweets(ArrayList<Tweet> allTweets) throws JSONException, IOException {
        //BUILD AN ARRAY
        JSONArray array = new JSONArray();
        for (Tweet c : allTweets)
            array.put(c.toJSON());

        //WRITE FILE TO DISK
        Writer writer = null;
        try {
            OutputStream out = mContext.openFileOutput(mFilename, Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    public ArrayList<Tweet> loadTweets() throws IOException, JSONException {
        ArrayList<Tweet> allTweets = new ArrayList<Tweet>();
        BufferedReader reader = null;
        try {
            //OPEN AND READ FILE INTO STRINGBUILDER
            InputStream in = mContext.openFileInput(mFilename);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                //line breaks are omitted and irrelevant
                jsonString.append(line);
            }
            //PARSE JSON USING JSONTokener
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
            //BUILD ARRAY OF TWEETS FROM JSON OBJECTS
            for (int i = 0; i < array.length(); i++) {
                allTweets.add(new Tweet(array.getJSONObject(i)));
            }
        } catch (FileNotFoundException e) {
            //IGNORE SINCE HAPPENS WHEN START FRESH
        } finally {
            if (reader != null)
                reader.close();
        }
        return allTweets;
    }
}
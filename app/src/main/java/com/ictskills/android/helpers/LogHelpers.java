package com.ictskills.android.helpers;

import android.util.Log;

public class LogHelpers {
    // Log helper to display messages for debugging/development
    public static void info(Object parent, String message) {
        Log.i(parent.getClass().getSimpleName(), message);
    }
}
